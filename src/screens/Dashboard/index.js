import {Footer, Header, PageTitle, ScrollTop, Sidebar} from "../../components";
import {Route} from "react-router-dom";

export default function Dashboard(props){
    return(
        <div id="container" className="effect aside-float aside-bright mainnav-lg navbar-fixed">
            <Header/>
            <div className="boxed">
                <h1>Dashboard</h1>
                <Sidebar/>
            </div>
            <Footer/>
            <ScrollTop/>
        </div>
    )
}