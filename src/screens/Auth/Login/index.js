import './login.css'

export default function Login(props){
    return (
        <div id="container" className="cls-container">
            <div id="bg-overlay" className="bg-img" style={{backgroundImage: 'url("assets/nifty/img/bg-img/bg-img-2.jpg")'}}/>
            <div className="cls-content">
                <div className="cls-content-sm panel">
                    <div className="panel-body">
                        <div className="mar-ver pad-btm">
                            <h3 className="h4 mar-no">Đăng nhập hệ thống</h3>
                        </div>
                        <form method="post">
                            <div className="alert alert-danger fade in">
                                <button className="close" data-dismiss="alert"><span>×</span></button>
                                <span>Email hoặc password không chính xác!</span>
                            </div>
                            <div className="form-group">
                                <input name="username" type="text" placeholder="Email" className="form-control" autoFocus/>
                            </div>
                            <div className="form-group">
                                <input name="password" type="password" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <div className="checkbox pad-btm text-left">
                                    <input defaultValue={1} id="demo-form-checkbox" className="magic-checkbox"
                                           type="checkbox" name="is_remember_me"/>
                                    <label htmlFor="demo-form-checkbox">Ghi nhớ</label>
                                </div>
                            </div>
                            <button className="btn btn-primary btn-lg btn-block" type="submit">Đăng nhập</button>
                        </form>
                    </div>
                </div>
            </div>
            <div className="demo-bg">
                <div id="demo-bg-list">
                    <div className="demo-loading"><i className="psi-repeat-2"/></div>
                    <img className="demo-chg-bg bg-trans" src="assets/nifty/img/bg-img/thumbs/bg-trns.jpg"
                         alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-1.jpg" alt="Background Image"/>
                    <img className="demo-chg-bg active" src="assets/nifty/img/bg-img/thumbs/bg-img-2.jpg"
                         alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-3.jpg" alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-4.jpg" alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-5.jpg" alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-6.jpg" alt="Background Image"/>
                    <img className="demo-chg-bg" src="assets/nifty/img/bg-img/thumbs/bg-img-7.jpg" alt="Background Image"/>
                </div>
            </div>
        </div>
    )
}