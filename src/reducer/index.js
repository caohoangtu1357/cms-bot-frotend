import { combineReducers } from "redux";
import category from "./category";
import user from "./user";
import authenticate from "./authenticate";
import course from "./course";
import lesson from "./lesson";
import alert from "./alert";
import trainingProgram from './trainingProgram';
import major from './major';
import schoolYear from './schoolYear';
import classStorage from  './class';
const rootReducer = combineReducers({
  category,
  authenticate,
  user,
  course,
  lesson,
  alert,
  trainingProgram,
  major,
  schoolYear,
  class : classStorage
});

export default rootReducer;
