import {
    LOGIN_FAILURE,
    LOGIN_SUCCESS,

    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,

    CHECK_IS_LOGGED_SUCCESS,
    CHECK_IS_LOGGED_FAILURE
 } from "../config/ActionTypes";
 
 const init = {
     data:null,
     error: null,
 };
 
 export default function authReducer(state= init, action){
     switch (action.type){
         //Login
        case LOGIN_SUCCESS:
            return {
            ...state,
            data:action.data
            };
            
        case LOGIN_FAILURE:
            return {
                ...state,
                error:action.error
            };
        
        case LOGOUT_SUCCESS:
            return {
                ...state,
                data:{}
            };
            
        case LOGOUT_FAILURE:
            return {
                ...state,
                error:action.error
            };
            
        case CHECK_IS_LOGGED_SUCCESS:
            return {
                ...state,
                data:action.data,
                error:null
            };
        case CHECK_IS_LOGGED_FAILURE:
            return {
                ...state,
                error:action.error,
                data:null
            };

         default:
             return state;
     }
 }