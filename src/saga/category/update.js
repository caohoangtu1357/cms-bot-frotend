import {takeEvery,call,put} from 'redux-saga/effects';
import {url} from '../../config/API';
import {UPDATE_CATEGORY,GET_ALL_CATEGORY} from '../../config/ActionTypes';
import axios from 'axios';
import FormData from 'form-data'

import {
    
} from '../../action/category';

import {
    addAlert,
    stopAlert
}from '../../action/alert';

function updateAsync(data){
    let dataForm = new FormData();
    dataForm.append('image', data.image);
    dataForm.append('name',data.name);
    return axios.put(url+"/category/update-category/"+data.id,dataForm,{ 
        headers: {
            'content-type': `multipart/form-data;`
        }
    }).then(res=>{
        return Promise.resolve(res.data);
    }).catch(err=>{
        return Promise.reject(err.response.data.message);
    });
}

const delay = time => new Promise(resolve => setTimeout(resolve, time));

function* update(actionData){
    try{
        yield call(updateAsync,actionData.category);
        yield put({type: GET_ALL_CATEGORY});
    }catch(err){
        yield put(addAlert(err));
        yield call(delay,5000);
        yield put(stopAlert());
    }

}

export function* watchUpdateCategory(){
    yield takeEvery(UPDATE_CATEGORY,update);
}
