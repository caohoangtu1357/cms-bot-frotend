import { takeEvery, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_CATEGORY,GET_ALL_CATEGORY } from "../../config/ActionTypes";
import axios from "axios";

import {} from "../../action/category";
import {
  addAlert,
  stopAlert
}from '../../action/alert';

function deleteAsync(id) {
  return axios
    .delete(url + "/category/delete-category/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      console.log(err.response.data.message)
      return Promise.reject(err.response.data.message);
    });
}

const delay = time => new Promise(resolve => setTimeout(resolve, time));

function* deleteCategory(id) {
  try{
    yield call(deleteAsync, id);
    yield put({type: GET_ALL_CATEGORY});
  }catch(err){
    yield put(addAlert(err));
    yield call(delay,5000);
    yield put(stopAlert());
  }

}

export function* watchDeleteCategory() {
  yield takeEvery(DELETE_CATEGORY, deleteCategory);
}
