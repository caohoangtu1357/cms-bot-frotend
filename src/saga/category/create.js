import {takeLatest,call,put,takeEvery} from 'redux-saga/effects';
import {url} from '../../config/API';
import {ADD_CATEGORY,GET_ALL_CATEGORY} from '../../config/ActionTypes';
import axios from 'axios';
import FormData from 'form-data'


import {
    addCategorySuccess,
    addCategoryFailure
} from '../../action/category';

import {
    addAlert,
    stopAlert
}from '../../action/alert';

function addAsync(data){
    let dataForm = new FormData();
    dataForm.append('image', data.image);
    dataForm.append('name',data.name);
    return axios.post(url+"/category/add-category",dataForm,{ 
        headers: {
            'content-type': `multipart/form-data;`
        }
    }).then(res=>{
        return Promise.resolve(res.data);
    }).catch(err=>{
        return Promise.reject(err.response.data.message);
    });
}

const delay = time => new Promise(resolve => setTimeout(resolve, time));


function* add(actionData){
    console.log(actionData.category);
    try{
        var data= yield call(addAsync,actionData.category);
        yield put(addCategorySuccess(data));
        yield put({type: GET_ALL_CATEGORY});
    }catch(err){
        yield put(addAlert(err));
        yield call(delay,5000);
        yield put(stopAlert());
    }
}

export function* watchAddCategory(){
    yield takeEvery(ADD_CATEGORY,add);
}
