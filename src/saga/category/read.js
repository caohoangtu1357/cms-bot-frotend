import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_CATEGORY, GET_AN_CATEGORY } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllCategoryFailure,
  getAllCategorySuccess,
  getAnCategoryFailure,
  getAnCategorySuccess,
} from "../../action/category";

function readAsync() {
  return axios
    .get(url + "/category/get-all-category")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllCategorySuccess(data));
  } catch (err) {
    yield put(getAllCategoryFailure(err));
  }
}

export function* watchGetAllCategory() {
  yield takeLatest(GET_ALL_CATEGORY, read);
}

function readByIdAsync(idCategory) {
  console.log("id in axios", idCategory);
  return axios
    .get(url + "category/get-category-by-id/" + idCategory)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* readById(action) {
  try {
    var data = yield call(readByIdAsync, action.id);
    yield put(getAnCategorySuccess(data));
  } catch (error) {
    yield put(getAnCategoryFailure(error));
  }
}

export function* watchGetAnCategoryById() {
  yield takeLatest(GET_AN_CATEGORY, readById);
}
