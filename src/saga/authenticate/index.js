import {all} from 'redux-saga/effects';
import {watchLogin} from "./login";
import {watchCheckIsLogged} from "./checkAuth";
import {watchLogout} from "./logout";

export default function* rootShoes(){
    yield all([
        watchLogin(),
        watchCheckIsLogged(),
        watchLogout()
    ])
}