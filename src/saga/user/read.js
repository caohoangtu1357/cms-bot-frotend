import { takeLatest, call, put, all } from "redux-saga/effects";
import { url } from "../../config/API";
import { GET_ALL_TEACHER, GET_ALL_USER, GET_AN_USER } from "../../config/ActionTypes";
import axios from "axios";

import {
  getAllTeacherSuccess,
  getAllUserFailure,
  getAllUserSuccess,
  getAnUserFailure,
  getAnUserSuccess,
} from "../../action/user";
import Axios from "axios";

function readAsync() {
  return axios
    .get(url + "/user/get-all")
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
}

function* read() {
  try {
    var data = yield call(readAsync);
    yield put(getAllUserSuccess(data));
  } catch (err) {
    yield put(getAllUserFailure(err));
  }
}

function* getAllTeacher(){
  const res = yield call(async () => {
    return await Axios.get(url + '/user/get-all-teacher');   
  });
  if(res.status == 200){
      yield put(getAllTeacherSuccess(res.data));
  }
  else
  {
      console.log(res.data);
  }
}

export function* watchGetAllUser() {
  yield all([
    yield takeLatest(GET_ALL_USER, read),
    yield takeLatest(GET_ALL_TEACHER, getAllTeacher)
  ])
 
}
