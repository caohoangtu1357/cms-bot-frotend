import { takeLatest, call, put } from "redux-saga/effects";
import { url } from "../../config/API";
import { DELETE_USER,GET_ALL_USER } from "../../config/ActionTypes";
import axios from "axios";
import {
  addAlert,
  stopAlert
}from '../../action/alert';

function deleteAsync(id) {
  return axios
    .delete(url + "/user/delete/" + id.id)
    .then((res) => {
      return Promise.resolve(res.data);
    })
    .catch((err) => {
      return Promise.reject(err.response.data.message);
    });
}

const delay = time => new Promise(resolve => setTimeout(resolve, time));

function* deleteUser(id) {
  try{
    yield call(deleteAsync, id);
    yield put({type: GET_ALL_USER});
  }catch(err){
    yield put(addAlert(err));
    yield call(delay,5000);
    yield put(stopAlert());
  }

}

export function* watchDeleteUser() {
  yield takeLatest(DELETE_USER, deleteUser);
}
