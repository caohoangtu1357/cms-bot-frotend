import { all } from "redux-saga/effects";
import rootCategory from "./category/index";
import rootUser from "./user/index";
import rootAuthenticate from "./authenticate/index";
import rootCourse from "./course/index";
import rootLesson from "./lesson/index";
import rootTrainingProgram from './trainingProgram';
import majorAndSchoolyear from './major-and-schoolyear';
import rootClass from './class';
export default function* rootSaga() {
  yield all([
    rootCategory(), 
    rootAuthenticate(), 
    rootUser(), 
    rootLesson(),
    rootCourse(),
    rootTrainingProgram(),
    majorAndSchoolyear(),
    rootClass()
  ]);
}
