export default function Footer(props){
    return(
        <footer id="footer">

            <div className="show-fixed pull-right">
                You have <a href="#" className="text-bold text-main"><span className="label label-danger">3</span> pending
                action.</a>
            </div>
            <p className="pad-lft">&#0169; CÔNG TY TNHH MTV VIỄN THÔNG QUỐC TẾ FPT</p>
        </footer>
    )
}


