import Sidebar from './SideBar';
import PageTitle from './PageTitle';
import Footer from './Footer';
import ScrollTop from './ScrollTop';
import Header from "./Header";
export {
    Sidebar,
    PageTitle,
    Footer,
    ScrollTop,
    Header
}