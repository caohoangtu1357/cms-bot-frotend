import {useState} from 'react';

export default function index(props) {
    return (
        <nav id="mainnav-container">
            <div id="mainnav">
                <div id="mainnav-menu-wrap">
                    <div className="nano">
                        <div className="nano-content">
                            <div id="mainnav-profile" className="mainnav-profile">
                                <div className="profile-wrap text-center">
                                    <div className="pad-btm">
                                        <img className="img-circle img-md" src="assets/images/user.png"
                                             alt="Profile Picture"/>
                                    </div>
                                    <a href="#profile-nav" className="box-block" data-toggle="collapse"
                                       aria-expanded="false">
                                            <span className="pull-right dropdown-toggle">
                                                <i className="dropdown-caret"></i>
                                            </span>
                                        <p className="mnp-name">Aaron Chavez</p>
                                        <span className="mnp-desc">aaron.cha@themeon.net</span>
                                    </a>
                                </div>
                                <div id="profile-nav" className="collapse list-group bg-trans">
                                    <a href="#" className="list-group-item">
                                        <i className="demo-pli-male icon-lg icon-fw"></i> View Profile
                                    </a>
                                    <a href="#" className="list-group-item">
                                        <i className="demo-pli-gear icon-lg icon-fw"></i> Settings
                                    </a>
                                    <a href="#" className="list-group-item">
                                        <i className="demo-pli-information icon-lg icon-fw"></i> Help
                                    </a>
                                    <a href="#" className="list-group-item">
                                        <i className="demo-pli-unlock icon-lg icon-fw"></i> Logout
                                    </a>
                                </div>
                            </div>
                            <ul id="mainnav-menu" className="list-group">
                                <li className="">
                                    <a href="">
                                        <i className="fa demo-pli-home" aria-hidden="true"></i>
                                        <span className="menu-title">
                                    <strong>Dashboard</strong>
                                </span>
                                    </a>
                                </li>
                                <li className="">
                                    <a href="#">
                                        <i className="fa fa-puzzle-piece" aria-hidden="true"></i>
                                        <span className="menu-title">
                                            <strong>Phân quyền</strong>
                                        </span>
                                        <i className="arrow"></i>
                                    </a>
                                    <ul className="collapse">
                                        <li className=""><a href="">Quản lý tài khoản</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}