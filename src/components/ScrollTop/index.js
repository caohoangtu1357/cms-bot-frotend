export default function Footer(props){
    return(
        <button className="scroll-top btn">
            <i className="pci-chevron chevron-up"></i>
        </button>
    )
}