export default function Header(props) {
    return (
        <header id="navbar">
            <div id="navbar-container" className="boxed">
                <div className="navbar-header">
                    <a href="" className="navbar-brand">
                        <img src="/assets/nifty/img/logo.png" alt="Nifty Logo" className="brand-icon"/>
                        <div className="brand-title">
                            <span className="brand-text">CMS - BOT</span>
                        </div>
                    </a>
                </div>
                <div className="navbar-content clearfix">
                    <ul className="nav navbar-top-links pull-left">
                        <li className="tgl-menu-btn">
                            <a className="mainnav-toggle" href="#">
                                <i className="demo-pli-view-list"/>
                            </a>
                        </li>
                    </ul>
                    <ul className="nav navbar-top-links pull-right">
                        <li id="dropdown-user" className="dropdown">
                            <a href="#" data-toggle="dropdown" className="dropdown-toggle text-right">
                              <span className="pull-right">
                                <img className="img-circle img-user media-object" src="/assets/images/user.png" alt=""/>
                              </span>
                                <div className="username hidden-xs">full name</div>
                            </a>
                            <div className="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">
                                <div className="pad-all bord-btm">
                                    <p className="text-main mar-btm">Xin chào,</p>
                                </div>
                                <ul className="head-list">
                                    <li>
                                        <a href="#">
                                            <i className="demo-pli-male icon-lg icon-fw"/> Thông tin của bạn
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <i className="fa fa-key"
                                               style={{fontSize: '16px', paddingRight: '4px'}}/> Thay đổi mật khẩu
                                        </a>
                                    </li>
                                </ul>
                                <div className="pad-all text-right">
                                    <a href="" className="btn btn-primary btn-rounded">
                                        <i className="demo-pli-unlock"/> Thoát
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    )
}