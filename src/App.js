import logo from './logo.svg';
import './App.css';
import { Redirect, Route, HashRouter as Router } from "react-router-dom";

import {Dashboard,Login} from "./screens"

function App() {
  return (
      <Router>
          <div>
              <Route
                  exact
                  path="/login"
                  render={()=>{
                      return <Login/>
                  }}
              />
              <Route
                  exact
                  path="/"
                  render={()=>{
                      return <Dashboard/>
                  }}
              />
          </div>
      </Router>
  );
}

export default App;
